import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";
import axios from "axios";

import "./home.css";

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products: [],
      count: 0,
      error: false,
      loading: true,
      type: null,
      page: 0,
      showBackButton: false,
      showNextButton: false,
      totalCount: 0
    };
  }

  componentDidMount() {
    this.loadData();
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (nextProps.location.pathname !== nextState.type) {
      this.loadData();
      return true;
    } else if (nextState.type) return true;
    return false;
  }

  loadData = () => {
    let type = null;
    if (this.props.history.location && this.props.history.location.pathname) {
      type = this.props.history.location.pathname;
    }
    let sendPromise = axios.get("/products", { params: {}});
    if (type) {
      sendPromise = axios.get("/products", {
        params: { type: type.split("/")[1], page: this.state.page }
      });
    }
    sendPromise.then(results => {
      if (results.error) {
        this.setState({ products: [], error: true, loading: false, type });
      } else {
        let { count, products, totalCount } = results.data;
        this.setState(
          {
            count,
            products,
            error: false,
            loading: false,
            type,
            totalCount
          },
          () => {
            if (this.state.page === 0)
              this.setState({ showBackButton: false, showNextButton: true });
            else if (
              this.state.page === parseInt(parseInt(this.state.totalCount) / 12)
            )
              this.setState({ showBackButton: true, showNextButton: false });
            else this.setState({ showBackButton: true, showNextButton: true });
            this.renderProducts()
          }
        );
      }
    });
  };

  renderProducts = () => {
    if(this.state.products.length === 0){
      return <div className="image-no-prod">No Product found</div>
    } 
    return this.state.products.map(product => {
      let id = product.tcin;
      let data = { pathname: "/product/" + id, productId: id };
      return (
        <div className="image-wrapper-show" key={id}>
          <Link to={data}>
            <img alt={product.title} className="img-wrapper" src={product.meta.img_url} />
            <div className="title-wrapper">{product.title}</div>
          </Link>
        </div>
      );
    })
  
  };

  clickNextHandler = () => {
    let page = this.state.page + 1;
    this.setState({ page }, () => {
      this.loadData();
    });
  };

  clickBackHandler = () => {
    if (this.state.page >= 0) {
      let page = this.state.page - 1;
      this.setState({ page }, () => {
        this.loadData();
      });
    }
  };

  renderPage = () => {
    if(this.state.products.length === 0) return <div></div>
    return (
      <div className="image-wrapper-main">
        {this.renderProducts()}
        <div className="button-div-wrapper">
          {this.state.showBackButton ? (
            <div
              onClick={() => this.clickBackHandler()}
              className="button-wrapper"
            >
              {"<<<"}Back
            </div>
          ) : null}
          {this.state.showNextButton ? (
            <div
              onClick={() => this.clickNextHandler()}
              className="button-wrapper"
            >
              Next{">>>"}
            </div>
          ) : null}
        </div>
      </div>
    );
  };

  render() {
    if (this.state.loading) {
      return <div>Loding...</div>;
    } else if (this.state.error && !this.state.loading) {
      return <div>Oops something went wrong</div>;
    } else if (this.state.products.length ===0) {
      return <div className="image-no-prod">Product not found</div>;
    }  else {
      return this.renderPage();
    }
  }
}

export default withRouter(Home);
