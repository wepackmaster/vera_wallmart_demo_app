import React, { Component } from "react";
import { withRouter, Link } from "react-router-dom";
import Axios from "axios";

import "./product.css";

class Product extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: false,
      loading: true,
      suggestion: [],
      id: null
    };
  }

  loadData = () => {
    let id = null;
    if (this.props.history && this.props.history.location.productId)
      id = this.props.history.location.productId;
    else id = this.props.history.location.pathname.split("/")[2];
    if (id) {
      Axios.get("/wallmart", { params: { id } }).then(result => {
        if (result.data.error) {
          this.setState({ error: true, loading: false, suggestion: [], id });
        } else {
          this.setState({
            error: false,
            loading: false,
            product: result.data.product,
            suggestion: result.data.result,
            id
          });
        }
      });
    }
  };

  componentDidMount() {
    this.loadData();
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (
      nextProps.location.productId &&
      nextProps.location.productId !== nextState.id
    ) {
      this.loadData();
      return true;
    } else if (nextState.id) return true;
    else return false;
  }

  renderSuggestion = () => {
    return this.state.suggestion.map(product => {
      let id = product.tcin;
      let data = { pathname: "/product/" + id, productId: id };
      return (
        <div className="image-wrapper-show" key={id}>
          <Link to={data}>
            <div className="margin-auto">
              <img  alt={product.title} src={product.meta.img_url} />
            </div>
            <div style={{ width: "80%", margin: "auto", textAlign: "center" }}>
              {product.title}
            </div>
          </Link>
        </div>
      );
    });
  };

  renderProduct = () => {
    let product = this.state.product;
    return (
      <div className="product-suggestion-wrapper">
        <div className="prodcut-image-wrapper">
          <img  alt={product.title} className="product-img-wrap" src={product.meta.img_url} />{" "}
        </div>
        <div className="product-detail-wrapper">
          <div>Product Name : {product.title}</div>
          <br />
          <div>Brand : {product.brand}</div>
          <br />
          <div>
            Available on : <a href={product.url}>{product.from}</a>
          </div>
        </div>
      </div>
    );
  };

  deepRenderSuggestion = () => {
    return (
      <div>
        {this.renderProduct()}
        <h3 style={{ paddingTop: "2em" }}>Suggestions:</h3>
        <div className="product-img">{this.renderSuggestion()}</div>
      </div>
    );
  };

  render() {
    if (this.state.loading) {
      return <div>Loding...</div>;
    } else if (this.state.error && !this.state.loading) {
      return <div>Oops something went wrong</div>;
    } else {
      return this.deepRenderSuggestion();
    }
  }
}

export default withRouter(Product);
