import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  NavLink
} from "react-router-dom";

import Home from "./components/home/home";
import Product from "./components/product/product";
import Config from "./config/config";
import "./App.css";

const renderNav = () => {
  return Config.navlist.map((data,index) => {
    return (
      <NavLink
        key={index}
        exact={true}
        className="nav-wrapper"
        activeClassName="is-active"
        to={data.value}
      >
        {data.key}
      </NavLink>
    );
  });
};


const renderDiv = ()=>{
  return Config.navlist.map((data,key)=>{
    return (<Route key={key} exact path={data.value}>
          <Home />
        </Route>)
  })
}

function App() {
  return (
    <Router>
      <div>
        <nav className="navbar-wrapper">{renderNav()}</nav>
        <div className="div-wrapper">
          <div className="image-wrapper-main">
            <Switch>
              <Route exact path="/">
                <Home />
              </Route>
              {renderDiv()}
              <Route path="/product/:id">
                <Product />
              </Route>
            </Switch>
          </div>
        </div>
      </div>
    </Router>
  );
}

export default App;
