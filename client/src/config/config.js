const Config = {
  navlist: [
    { key: "Home", value: "/" },
    { key: "Wallmart", value: "/wallmart" },
    { key: "Zara", value: "/zara" }
  ]
};

export default Config;
