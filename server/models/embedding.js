const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const EmbeddingSchema = new Schema({
  tcin: { type: String },
  embedding: { type: Array }
});

module.exports = mongoose.model("embedding", EmbeddingSchema, "embedding");
