const rec = require("./server/recommendation");
const express = require("express");
const mongoose = require("mongoose");
const app = express();
const cors = require("cors");
const path = require("path");

const Product = require("./server/models/product");
const config = require("./server/config/config");

const port = process.env.port || config.port;

mongoConnect = async () => {
  await mongoose
    .connect(config.mongodb, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    })
    .then(() => console.log("connected"))
    .catch(e => console.error(e));
};

mongoConnect();

app.use(
  cors({
    origin: "*"
  })
);

app.use(express.static(path.join(__dirname, "/client/build")));

app.route("/products").get((req, res) => {
  const type = req.query.type || null;
  const skip = (parseInt(req.query.page) + 1) * 12 || 0;
  if (config.productType.includes(type)) {
    let sendPromise = Product.find({})
      .limit(12)
      .skip(skip);
    let countPromise = Product.find({}).count();
    if (type) {
      sendPromise = Product.find({ from: type })
        .limit(12)
        .skip(skip);
    }

    let promiseArr = Promise.all([sendPromise, countPromise]);

    promiseArr
      .then(products => {
        res.send({
          products: products[0],
          length: products[0].length,
          totalCount: products[1]
        });
      })
      .catch(e => {
        res.send({ result: "Not Found", error: true });
      });
  } else {
    Product.aggregate([{ $sample: { size: 12 } }])
      .then(products => {
        res.send({ products, count: products.length });
      })
      .catch(e => {
        res.send({ results: { products: "Not Found", error: true } });
      });
  }
});

app.route("/wallmart").get((req, res) => {
  const tcin = req.query.id;
  const val = rec.top_to_wallmart(tcin);
  if (val.error) {
    res.send({ result: "Not Found", error: true });
  } else {
    Product.find({
      tcin: { $in: val }
    })
      .then(result => {
        let product = null;
        result.map(e => {
          if (e.tcin === tcin) product = e;
        });
        let productArr = [];
        result.map(e => {
          if (e.tcin !== tcin) productArr.push(e);
        });

        res.send({ product, result: productArr, length: result.length });
      })
      .catch(e => {
        res.send({ result: "Not Found", error: true });
      });
  }
});

if (module === require.main) {
  const server = app.listen(process.env.PORT || 8080, () => {
    const port = server.address().port;
    console.log(`App listening on port ${port}`);
  });
}

module.exports = app;

// app.listen(port, () => {
//   console.log("Listening at port 5000");
// });
